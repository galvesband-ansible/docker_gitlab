Ansible Role - Gitlab on Docker
=========

Launchs a GitLab instance on Docker on Ubuntu 20.04. This is meant 
to be used in conjunction with other roles in 
[this gitlab group](https://gitlab.com/galvesband-ansible).

Gitlab is a complex beast. This role uses the containerized version
of Gitlab to create an instance. Most of the effort in the role is
going towards SSL management. The final objective is to be able
to run Gitlab and its registry, with some runners elsewhere,
all with certificates signed by a custom certificate authority.

With that in mind, this role aims to support two basic working modes:

 - Normal mode. Gitlab exposes ports 80 and 443 and uses a custom
   certificate. The container registry will be exposed in 5050
   by default, and will use the same certificate that the main
   gitlab entrypoint. Everything will be SSL by gitlab's own
   means.

 - Reverse proxy mode. In this mode, gitlab relinquish SSL 
   termination in favor of a reverse proxy (not provided
   by this role).

There is support for custom public certificate in the role's
configuration.

Requirements
------------

None.


Role Variables
--------------

 - `gitlab_container_version`: Version of the container image in docker-hub. 
   Defaults to `13.9.1-ce.0` right now but will probably be bumped up soon.

 - `gitlab_timezone`: Time zone in gitlab's configuration. By default 
   `Europe/Madrid`.

 - `gitlab_ssh_port`: External ssh port for gitlab. Keep in mind that ussually
   the server's ssh port (22) is occupied by the server's ssh. Either move
   system's ssh port to other port and set gitlab's port to 22 or set up 
   gitlab's port in any other port. Defaults to `2222`.

 - `gitlab_fqdn`: DNS name of the instance. Obviously you need to set up some
   for of name resolution to be able to just type this on a browser and get
   to the instance. Suggestions: add the instance ip to `/etc/hosts` or
   look at `https://nip.io`. Defaults to `gitlab.local`.

 - `gitlab_registry_fqdn`: DNS name of the docker registry. In reverse
   proxy mode, it ussually is different from `gitlab_fqdn`. When not in 
   reverse proxy mode it should probably be the same as `gitlab_fqdn`.
   Defaults to `{{ gitlab_fqdn }}`.

 - `gitlab_reverse_proxy_mode`: `true` or `false`. 
   - When not in reverse proxy mode, ussually you want to expose directly
     gitlab with its own SSL termination and certificates. SSL is enabled
     and certificates installed in `/srv/gitlab/config/ssl/<fqdn>.crt` 
     and `/srv/gitlab/config/ssl/<fqdn>.crt` will be used (if they match
     gitlab or the registry fqdns).
   - When in reverse proxy mode, gitlab's SSL termination is disabled and
     only the http ports are exposed. Gitlab is configured to trusts
     proxyes in 172.0.0.0/8, 192.168.0.0/16 and 127.0.0.0/8 ranges and
     expects the ussual proxy headers.

  - `gitlab_http_port`: Port for main http entrypoint. In reverse proxy mode
    this will serve unencrypted http requests to gitlab. When not in 
    reverse proxy, it will redirect to the https port.

  - `gitlab_https_port`: Port for main https entrypoint. In reverse
    proxy mode it is disabled and not exposed. Otherwise, servers https
    requests signed by a self signed certificate or a certificate provided
    that matches `gitlab_fqdn` name in `/srv/gitlab/config/ssl/`.

  - `gitlab_registry_port`: Port where the docker registry will be exposed.
    Defaults to `5050`.

  - `gitlab_fetch_certs`: This array allows to specify locations and a remote 
    host where a certificate is located. The certificate will be imported
    into `/srv/gitlab/config/ssl`. Defaults to `[]`. 
    Elements in the list should have this properties:
     - `cert`: Path to the `.crt` file in the remote server.
     - `key`: Path to the `.key` file in the remote server.
     - `fqdn`: DNS name that the certificate is valid for. Ussually, this 
       would be `gitlab_fqdn` and, maybe, `gitlab_registry_fqdn` if they 
       don't have the same value.
     - `host`: Inventory name of the machine that has the certificate.

  - `gitlab_fetch_trusted_ca`: In this array you can specify a list of 
    public certificates to add to the internal gitlab trusted certificates
    collection. Each element of the list should have:
    - `path`: Path in the remote server to the `.ca` file.
      `host`: Inventory name of the machine that has the certificate.


Dependencies
------------

This role is meant to work integrated with other roles in [this 
gitlab group](https://gitlab.com/galvesband-ansible). The role
does not have hard dependencies on any of them though, but
needs a docker installation, for example. To see a complet example, 
look at the [converge](molecule/reverse_proxy/converge.yml) file
of the `reverse_proxy` scenario and the 
[requirements](requirements.yml) file.


Example Playbook
----------------

The following example uses other roles from the 
[gitlab group](https://gitlab.com/galvesband-ansible) to set up 
a two server play were one server holds a simple certificate 
authority and the others runs docker and gitlab with a 
certificate signed by the CA on the first server.

Keep in mind that this is not a production-ready configuration. 
In particular, the certificate authority and certificates 
generated by the next play are only suitable for experiments or 
local or private networks.

```yml
---

- name: MySimpleCertificateAuthority
  hosts: my-secure-server
  roles:

    # Builds a simple Certificate Authority in my-secure-server
    - name: certificate_authority
      vars:
        ca_name: my-certificate-authority

- name: MySimpleGitlabPlay
  hosts: my-gitlab-server
  roles:

    # Build a certificate in this server, signed by the CA
    # in the other server, and stores it in this server.
    - name: ca_based_certificate
      vars: 
        ca_based_certificate_fqdn: my-local-gitlab.local
        ca_based_certificate_ca_name: my-certificate-authority
        ca_based_certificate_ca_host: my-secure-server

    # Installs docker
    - name: docker
      vars:
        # We set up docker so that it trusts gitlab's registry
        # directly. With this approach the OS doesn't need to
        # trust our custom CA to operate with the registry.
        docker_trusted_registries:
          - fqdn: my-local-gitlab.local
            port: 5050
            cert_src: /srv/certs/my-local-gitlab.local/cert.crt
            cert_host: my-gitlab-server

    # Sets up gitlab on docker
    - name: docker_gitlab
      vars:
        gitlab_timezone: Europe/Madrid
        # We make the role use the certificate we previously generated.
        # Gitlab will be exposed through https with this certificate.
        gitlab_fetch_certs:
          - cert: /srv/certs/my-local-gitlab.local/cert.crt
            key: /srv/certs/my-local-gitlab.local/cert.key
            fqdn: my-local-gitlab.local
            host: my-gitlab-server
```

Anothe example, using the reverse-proxy mode with another role providing
the reverse proxy.

```yml
---
- name: ReverseGitLab
  hosts: my-big-server
  vars: 
    fqdn: gitlab.local
    registry_fqdn: registry.local
  tasks:

    # Builds a simple Ceritification Authority in this server.
    - name: "Generate a simple CA"
      include_role: 
        name: certificate_authority
      vars:
        ca_name: testca

    # Builds a web certificate signed by our CA for gitlab.
    - name: "Generate an SSL certificate"    
      include_role: 
        name: ca_based_certificate
      vars: 
        ca_based_certificate_fqdn: "{{ fqdn }}"
        ca_based_certificate_ca_name: testca
        ca_based_certificate_ca_host: my-big-server

    # Builds a web certificate signed by our CA for gitlab's container registry.
    - name: "Generate an SSL certificate"    
      include_role: 
        name: ca_based_certificate
      vars: 
        ca_based_certificate_fqdn: "{{ registry_fqdn }}"
        ca_based_certificate_ca_name: testca
        ca_based_certificate_ca_host: my-big-server

    # Installs our CA public certificate in the OS. Now Docker
    # will trust gitlab's registry.
    - name: "Install CA in OS"
      include_role:
        name: install_ca
      vars:
        install_ca_certificates:
        - ca_name: testca
          path: /srv/ca/testca/ca.crt
          host: my-big-server

    # Installs docker
    - name: "Install docker"
      include_role:
        name: docker

    # Launchs gitlab in docker
    - name: "Include docker_gitlab"
      include_role:
        name: "docker_gitlab"
      vars:
        gitlab_fqdn: "{{ fqdn }}"
        gitlab_registry_fqdn: "{{ registry_fqdn }}"
        gitlab_reverse_proxy_mode: true
        gitlab_http_port: 8000
        gitlab_registry_port: 5050
        # This will fetch our custom ca public key and add it
        # to gitlab private certificate collection. This will allow 
        # secure communication between gitlab's and its own
        # registry, that tecnically have different names.
        gitlab_fetch_trusted_ca:
          - path: "/srv/ca/testca/ca.crt"
            host: my-big-server
      tags: ["gitlab"]
    
    - name: "Set up reverse proxy"
      include_role:
        name: "reverse_proxy"
      vars:
        reverse_proxy_sites:
          default:
            ssl: false

          gitlab.local:
            domains: 
              - "{{ fqdn }}"
            upstreams:
              - { backend_address: localhost, backend_port: 8000 }
            ssl: true
            ssl_fetch_cert: true
            ssl_fetch_cert_host: my-big-server
            ssl_fetch_cert_path: "/srv/certs/{{ fqdn }}/cert.crt"
            ssl_fetch_cert_key_path: "/srv/certs/{{ fqdn }}/cert.key"

          registry.local:
            domains: 
              - "{{ registry_fqdn }}"
            upstreams:
              - { backend_address: localhost, backend_port: 5050 }
            ssl: true
            ssl_fetch_cert: true
            ssl_fetch_cert_host: my-big-server
            ssl_fetch_cert_path: "/srv/certs/{{ registry_fqdn }}/cert.crt"
            ssl_fetch_cert_key_path: "/srv/certs/{{ registry_fqdn }}/cert.key"

```

License
-------

[GPL 2.0 Or later](https://spdx.org/licenses/GPL-2.0-or-later.html).
